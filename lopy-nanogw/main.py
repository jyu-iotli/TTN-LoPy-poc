import pycom,time,config
from nanogateway import NanoGateway

off,red,green,blue=[0x000000,0xff0000,0x00ff00,0x0000ff]

pycom.heartbeat(False)

pycom.rgbled(green)
time.sleep(0.3)
pycom.rgbled(off)
time.sleep(0.3)

nanogw = NanoGateway(id=config.GATEWAY_ID, frequency=config.LORA_FREQUENCY,datarate=config.LORA_DR, ssid=config.WIFI_SSID,password=config.WIFI_PASS, server=config.SERVER,port=config.PORT, ntp=config.NTP, ntp_period=config.NTP_PERIOD_S)

pycom.rgbled(green)
time.sleep(1)
pycom.rgbled(off)

nanogw.start()
