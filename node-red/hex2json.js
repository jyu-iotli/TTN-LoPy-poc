var tempPayload = "{\"error\" : \"Invalid type\"}";
var humiPayload = "{\"error\" : \"Invalid type\"}";
var typeString = "" + msg.payload[0] + msg.payload[1] + msg.payload[2] + msg.payload[3];
if (typeString == "116101104117"){
    tempPayload = "{\"type\" : " + "\"temperature\"," +
        "\"sensorid\" : " + msg.payload[10] + "," +
        "\"value\" : " + msg.payload[4] + msg.payload[5] + "." + msg.payload[6] + "," +
        "\"timestamp\" : " + "\"" + new Date().toISOString() + "\"}";
    humiPayload = "{\"type\" : " + "\"humidity\"," +
        "\"sensorid\" : " + msg.payload[10] + "," +
        "\"value\" : " + msg.payload[7] + msg.payload[8] + "." + msg.payload[9] + "," +
        "\"timestamp\" : " + "\"" + new Date().toISOString() + "\"}"
}
var tempMsg = { payload: tempPayload };
var humiMsg = { payload: humiPayload };
return [ tempMsg, humiMsg, debugMsg ];