# TTN LoPy proof of concept demo
This repository contains configurations, code and documentation of first
LoRa / The Things Network demo. 

This setup uses BLE sensors and [PyCom LoPy](https://www.pycom.io/product/lopy/) chips to track 
temperature & humidity. Values are sent over LPWAN network (LoRa)
to [The Things Network](https://www.thethingsnetwork.org/) messaging platform.

Node-red parses LoRa messages from TTN to JSON and adds received values to
Elasticsearch database. Kibana is used to visualize gathered data.

## BLE temperature sensors
Temperature and humidity is measured with TokenCube BLE sensor. 
These sensors were chosen to the demo setup because of their low power usage 
and long lasting battery (up to several years).

Bluetooth Low Energy is ideal as a short range wireless protocol when
it is used together with LPWAN such as LoRa.

TokenCube sensor broadcasts periodic (1-3s) BLE advertisement packages which 
include temperature, humidity, orientation and battery information.

## PyCom LoPy BLE sniffer
LoPy node listens for BLE advertisement packages sent by TokenCube sensor.
These packages are decoded and temperature and humidity values are stored in node.

Every one minute (by default) current values are sent to TTN with LoRa.

References to get started with LoPy:
```
https://docs.pycom.io/
https://www.thethingsnetwork.org/docs/devices/lopy/usage.html
```

### Payload format
Message sent from the node is in hex format. This hex message is
decoded to decimal payload in Node-Red. 

First 4 bytes tell what type of values current payload has.
In this case payload contains temperature and humidity values so
the first 4 bytes are "tehu" (temperature, humidity).

Next 3 + 3 bytes are the first and second values. Due to limitation in
Node-red, bytes containing actual values are in 3 bytes instead of 2.
Maybe this could be fixed in the future?

Last byte is "sensorid" which identifies the device that has sent the payload.

Hex payload format:
```
raw hex payload
[ 0x74 0x65 0x68 0x75 0x02 0x02 0x05 0x04 0x05 0x01 0x01 ]

decoded to ascii / decimal
[ t    e    h    u    2    2    5    4    5    1    1 ]
```
## PyCom LoPy NanoGW
In this setup, an extra LoPy is used as a "nano gateway" to provide
connectivity between LoRa messages and The Things Network backend.

Nano gateway was built by following tutorial:
```
https://docs.pycom.io/chapter/tutorials/lopy/lorawan-nano-gateway.html
```

## Node-red
Node-red is used to receive raw LoRa messages from TTN.
LoRa messages are then parsed from raw hex payload to JSON. This JSON
is added to Elasticsearch database.

Simple tutorial for TTN and Node-red for reference:
```
https://www.thethingsnetwork.org/docs/applications/nodered/quick-start.html
```

![Node-red flow](images/node-red.png)

### Installation, dependencies and deployment
1. Install node-red and dependency libraries (make sure nodejs is installed before)
```
sudo npm install -g --unsafe-perm node-red
npm install node-red-contrib-ttn
```

2. Start node-red and import flow
```
menu > import > clipboard
paste flow configurations from /node-red/ttn-elastic-flow.txt
```

3. Configure TTN node settings in node-red
```
AppID, Access key, Broker
```

4. Deploy flow

## Elasticsearch & Kibana
When node-red flow is deployed and message is received,
message is parsed to two different JSON objects (temperature and humidity).

These JSON files are stored on Elasticsearch database in ttn/values indice. 
Elasticsearch must be running on the same device with node-red if default flow
configurations are used.

Kibana can be used to easily visualize gathered data from Elasticsearch. 

### Installation and deployment
Reference for installation and deployment of Elasticsearch / Kibana:
```
https://www.elastic.co/guide/en/elasticsearch/reference/current/setup.html
https://www.elastic.co/guide/en/kibana/current/setup.html
```
```
// Kibana default port
localhost:5601

// Elasticsearch default port
localhost 9200
```
### Value payload formats
Used JSON payload formats are defined in node-red function "hex2json".
Example of temperature payload:
```JSON
{
  "type": "temperature",
  "sensorid": 1,
  "value": 26.0,
  "timestamp": "2017-07-10T09:52:92.174Z"
}
```

Timestamp is stored in ISO 8601 format to achieve compatibility with
Elasticsearch indexing.