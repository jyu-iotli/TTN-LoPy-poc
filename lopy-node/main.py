# Pycom LoPy script that listens for BLE
# advertisement packages sent by TokenCube BLE
# sensor.
# 12.07.2017

import time,binascii,pycom,socket,struct
from network import Bluetooth,LoRa

bluetooth = Bluetooth()         # Initialize Bluetooth.

# Initialize LoRa
lora = LoRa(mode=LoRa.LORAWAN)
lora_freq,datarate_min,datarate_max=[868100000,0,5]
lora_socket=0
sendInterval=60 #sec

sensor="cf080a3d31c7" # TokenCube sensor MAC

# led colors
off,red,green,blue,orange=[0x000000,0xff0000,0x00ff00,0x0000ff,0xff6600]

# ABP authentication params (found on TTN)
dev_addr = struct.unpack(">l", binascii.unhexlify('26011E61'))[0]
nwk_swkey = binascii.unhexlify('6E8CE8EE14ADC91BFC61982E1C0092D5')
app_swkey = binascii.unhexlify('92832A3D39534A05324684D6B3E5AA0B')

# main function
def main():
    global wlan,bluetooth #wlan found in boot.py
    global sensor,lora_socket,sendInterval
    global off,red,green,blue,orange
    lastsendtime=30
    temperature,humidity=[999,999]
    while True:
        bluetooth_start()
        if not wlan.isconnected():
            wlan_start() #found in boot.py
        while bluetooth.isscanning() and wlan.isconnected():
            adv=bluetooth.get_adv()
            if adv:
                mac=str(binascii.hexlify(adv.mac))
                if sensor in mac:
                    led_onoff(blue,0.1,0)
                    data = bluetooth.resolve_adv_data(adv.data, Bluetooth.ADV_MANUFACTURER_DATA)
                    if data:
                        temp,humi=parse_temphumi(binascii.hexlify(data))
                        if temp!=999 and humi!=999:
                            temperature=temp
                            humidity=humi
                            print(sensor+": temp="+str(temp)+", humidity="+str(humi))
            else:
                time.sleep(.1)
            # lora_send
            if lastsendtime+sendInterval<time.time() and temperature!=999 and humidity!=999:
                try:
                    temp1=int((temperature/10)%10)
                    temp2=int(temperature%10)
                    temp3=int((temperature%1+0.0001)*10)
                    humi1=int((humidity/10)%10)
                    humi2=int(humidity%10)
                    humi3=int((humidity%1+0.0001)*10)
                    lora_socket.send(b'tehu'+bytes([temp1,temp2,temp3,humi1,humi2,humi3,0x01]))
                    print("Lora package sent.")
                    led_onoff(green,0.2,0.2)
                    led_onoff(green,0.5,0.2)
                except:
                    print("Error in lora_send.")
                    led_onoff(red,0.2,0.2)
                    led_onoff(red,0.2,0.2)
                    led_onoff(red,0.5,0.2)
                lastsendtime=time.time()

# function to parse temperature and humidity data from bluetooth hex-data
def parse_temphumi(s):
    temp,humi=[999,999]
    try:
        s=str(s)
        a=s.find("eeff0401")
        a2=hex_splitter(s[a+8:])
        i=1
        #print(a2)
        while i<len(a2):
            #temperature
            if a2[i]=="01" and i+2<len(a2):
                temp=int(a2[i+1]+a2[i+2], 16)/100.0
                i+=3
            #humidity
            elif a2[i]=="04" and i+2<len(a2):
                humi=int(a2[i+1]+a2[i+2], 16)/100.0
                i+=3
            #pressure
            elif a2[i]=="05" and i+4<len(a2):
            #    pres=int(a2[i+1]+a2[i+2]+a2[i+3]+a2[i+4], 16)/100.0
                i+=5
            #orientation
            elif a2[i]=="06":
                i+=7
            #pir
            elif a2[i]=="07":
                i+=2
            #pir
            elif a2[i]=="08":
                i+=2
            #shock
            elif a2[i]=="09":
                i+=4
            #battery
            elif a2[i]=="0A" and i+1<len(a2):
            #    bat=int(a2[i+1], 16)
                i+=2
            else:
                break
    except:
        print("Error in parse_temphumi().")
    return temp,humi#,pres,bat

# function to split hex-string to list by two character
def hex_splitter(s):
    ret=[]
    s2=s
    while len(s2)>=2:
        ret.append(s2[0:2])
        s2=s2[2:]
    return ret

# function to start wlan
def lora_start():
    global lora,lora_socket,lora_freq,datarate_min,datarate_max
    global dev_addr,nwk_swkey,app_swkey
    global off,red,green,blue,orange
    
    led_onoff(blue,0.2,0.3)
    
    # join a network using ABP (Activation By Personalization)
    lora.join(activation=LoRa.ABP, auth=(dev_addr, nwk_swkey, app_swkey))
    print("Joined LoRa network.")
    
    # remove all the non-default channels
    for i in range(3, 16):
        lora.remove_channel(i)
    
    # set the 3 default channels to the same frequency
    lora.add_channel(0, frequency=lora_freq, dr_min=datarate_min, dr_max=datarate_max)
    lora.add_channel(1, frequency=lora_freq, dr_min=datarate_min, dr_max=datarate_max)
    lora.add_channel(2, frequency=lora_freq, dr_min=datarate_min, dr_max=datarate_max)
    
    # create a LoRa socket
    lora_socket = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
    
    # set the LoRaWAN datarate
    lora_socket.setsockopt(socket.SOL_LORA, socket.SO_DR, datarate_max)
    
    # make the socket blocking
    lora_socket.setblocking(False)
    
    led_onoff(green,0.3,0.7)

def bluetooth_start():
    global bluetooth
    while True:
        if not bluetooth.isscanning():
            bluetooth.start_scan(-1) # start scanning with no timeout
            print("bluetooth scanning started.")
            break
        else:
            print("bluetooth already started. stopping bluetooth.")
            bluetooth.stop_scan()
            print("bluetooth stopped.")

# function to set led on/off
def led_onoff(color,ontime,offtime):
    global off
    if ontime>=0:
        pycom.rgbled(color)
        time.sleep(ontime)
    if offtime>=0:
        pycom.rgbled(off)
        time.sleep(offtime)



lora_start()
led_onoff(orange,0.3,0.3)
main()
