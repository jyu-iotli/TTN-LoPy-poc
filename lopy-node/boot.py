import time,pycom

from network import WLAN
wlan = WLAN(mode=WLAN.STA)  # Initialize WLAN.

WIFI_SSID = '5kTestipenkki'
WIFI_PASS = '5Kprokkis'

off,red,green,blue,orange=[0x000000,0xff0000,0x00ff00,0x0000ff,0xff6600]
pycom.heartbeat(False)

# function to set led on/off
def led_onoff(color,ontime,offtime):
    global off
    if ontime>=0:
        pycom.rgbled(color)
        time.sleep(ontime)
    if offtime>=0:
        pycom.rgbled(off)
        time.sleep(offtime)

# function to start wlan
def wlan_start():
    global wlan,WIFI_SSID,WIFI_PASS
    global off,red,green,blue,orange
    
    led_onoff(blue,0.2,-1)
    nets = wlan.scan()
    led_onoff(blue,-1,0.3)
    
    for net in nets:
        if net.ssid == WIFI_SSID:
            print('Network found!')
            wlan.connect(net.ssid, auth=(net.sec, WIFI_PASS), timeout=5000)
            while not wlan.isconnected():
                led_onoff(red,0.3,0.7)
            print('WLAN connection succeeded!')
            led_onoff(green,0.3,0.7)
            break

led_onoff(orange,0.3,0.3)
led_onoff(orange,0.3,0.3)

wlan_start()
time.sleep(7)
led_onoff(orange,0.7,0.3)
